# Detroit: Become Human savestate manager
[![platform](https://img.shields.io/badge/platform-linux-lightgrey.svg)](https://en.wikipedia.org/wiki/Linux)
[![distribution services](https://img.shields.io/badge/distribution%20services-steam-blue)](https://store.steampowered.com/)
[![python](https://img.shields.io/badge/python-3.6%20or%20above-green)](https://wiki.archlinux.org/title/Python)

A python script that provides a save state functionality for Detroit: Become Human.  
Based on SessionAutoSaveManager 1.01 (July 25, 2020) project.  

## Overview

  The developers of Detroit: Become Human try to discourage users from reloading checkpoints. They do this in a number of ways:

  - Chapter milestone checkpoints only become available once the next chapter has started.
  - Milestone checkpoints only appear in the Main Menu Chapter flowchart
    
  However the game also contains a large number of technical checkpoints that you do not normally have access to. You see such checkpoints are reached when a blue spinner (like a led on androids heads) in the upper right corner. They are spread out so that if you quit at any arbitrary moment, and then choose continue in the main menu, you generally won't lose more than 5 minutes of play time.

  This script provides ability to jump to such techinical checkpoints by backing up the games save game folder every 5 minutes. On startup, this app presents a list of backups that you can restore. So if you make a decision you regret, or lose a quicktime fight, or simply want to see other dialogs without replaying a long chapter, you can quit out and load one of the archived saves on the next startup.
  
  The app also supports the concept of sessions. Sessions group saves together so that you can play 2 very different characters in parallel or maybe you live in a house and several people share the same computer. Sessions can allow two seperate people to play their own games without stepping on each others toes.

  This is a must have script for those who want to unlock all nodes in the game. It simply saves lots of your time.
  
## Usage
       
### Basic Usage:

`python dbh-savestate-manager.py`

The first time the app runs, it will immediately start the game as there will not be any autosaves to present.

Later, when you start the app after having played a while, you will see a list of recent autosaves. It will look something like:

```
====================================================================
            Detroit : Become Human savestate manager
                    Session : [default]
====================================================================
[INFO] Auto Save every : [5] minute(s)
[INFO] Maximum Saves   : [50]
[INFO] Current Saves   : [12]

q) quit the script
d) open autosave folder in dolphin
0) Continue from most recent
1) Chapter1 Big Choice
2) $2021-06-16 22:45:39
3) $2021-06-16 22:22:23
4) $2021-06-16 22:17:49
5) $2021-06-16 22:12:43
6) $2021-06-16 22:07:36
7) $2021-06-16 22:02:30
8) $2021-06-16 21:57:23
9) Next

Select Option:
```

To skip save game restore, just hit ENTER or enter the value 0 and the session/game will start where you left off.
     
Warning! Keep in mind that savestates are like in a time machine! They also store your main flowchart. So be careful when you are loading your previous savestates after you finished some chapter and unlocked some nodes. Because it is easy to accidently start playing from checkpoint that had such nodes closed (at the moment of taking such savestate), so you effectively close them again.

I do not know it it is possible to exclude main flowchart from savestates.

I recommend deleting your previous savestates after you unlocked some nodes to prevent such situations.
     
### [Optional] Renaming loaded saves

You can choose to rename autosaves when you load them. For example, you could give it a distinctive name like "CHAPTER1_before_big_choice"

When dbh-savestate-manager.py goes to clean up the autosave directory, it will only remove directories that start with '$'. So as long as you do not start your name with a "$", it will not get removed and it will not count towards your max save games.

Save games are listed sorted by creation date with the most recent listed first. This is also true for named save games. (Over time, they will move to the end of the list).

PRO TIP1: Each save is about 25MB. So be mindfull of how many permanent saves you create. It adds up quick.

PRO TIP2: If you prefer more direct access to savestates, open the autosaves folder by choosing d option, it will open the folder in Dolphin.
This will list the sessions. ("default" if you are not using sessions). Enter the session and you can rename the directories all you want. 

### Using several sessions
```
-s <SESSIONNAME>
   For example:
   dbh-savestate-manager.py -s VIOLENT
```
This will tell the app to search for and store saves in the VIOLENT session (subdirectory of autosaves). The default session name is "default".

When creating a new session (first run with a specified session name), the game may act like it is starting up for the first time, unless it is the only session (you don't have a default). In that case, your session will use the current default game.

### [OPTIONAL] Copying the default Session Save(s) to Another Session

Behind the scenes, this script simply manages the directory:

`$HOME/.local/share/Steam/steamapps/compatdata/1222140/pfx/drive_c/users/steamuser/Saved Games/Quantic Dream/autosaves`
   
Within, you will see a folder/directory for each Session. Once you have ran the app once and established a default session, you can copy/clone sessions using File Explorer. IE:

- Paste the path above into Dolphin Location Bar. Hit Enter
- Right click the "default" directory and select copy.
- Right click the autosaves directory (or the background) and select paste.
- Rename the copied folder to the name you want for your session.

IE: Folder contents before:

.../autosaves/default

IE: Folder contents after:

.../autosaves/default  
.../autosaves/mySession  

### Other options

```
 -m <MAXSAVES>
     Will enforce the specified number of max saves.
     For example:
     dbh-savestate-manager.py.py -m 15

     The above would change the max saves to 15. You can use the value 0 to indicate that you never want to delete saves. 
    
 -f <FREQMINUTES>
     Will change how often autosaves are generated.
     For example:
     dbh-savestate-manager.py.py -f 4
     
     The above would cause the autosaver to create saves every 4 minutes instead of every 5 minutes (which is the default).

 -e "PATH/TO/DetroiteBecomeHuman.exe"
     <s>Override executable path</s>
     Currently used for command to run game.
     For example:
     dbh-savestate-manager.py -e "another command to start DetroitBecomeHuman"
     
A FULL EXAMPLE:
dbh-savestate-manager.py -s PROHUMAN -m 15 -f 4    
This would create a new session that only maintains 15 backups spaced by 4 min intervals (1 hour of gameplay).
```

## Deleting savestates

1. Run the script one last time and make sure you have the save loaded that you want.
1. Delete the autosaves directory:
Paste the following into the Dolphin File Manager:
`$HOME/.local/share/Steam/steamapps/compatdata/1222140/pfx/drive_c/users/steamuser/Saved Games/Quantic Dream/`
You should see the subdirectory "autosaves".
1. Delete the subdirectory. Note this will delete all sessions.

## Contribution
This script was made for you by Andrew Shark.  
The repository is located here: https://gitlab.com/AndrewShark/dbh-savestate-manager  

The author of original script is Dheu  (Email:  Dheuster@gmail.com, Use subject:  SessionAutoSaveManager 1.01)  
Original url: https://www.nexusmods.com/detroitbecomehuman/mods/10?tab=description.  
