#!/usr/bin/env python

# TO CONVERT THIS FILE TO EXECUTABLE:
# > pip install pyinstaller
# > pyinstaller --onefile SessionAutoSaveManager.py

import psutil
from wmctrl import Window

###############################################################################
# OVERVIEW:
###############################################################################

def overview():
    print('''
Usage:
  python dbh-savestate-manager.py [-h] [-s SESSION_NAME] [-m MAXSAVES] [-f FREQ] [-e EXEPATH]

Options:
  -h      : help
  -s      : provide alt session name (default is "default")
  -f      : auto save every FREQ (default 5) minutes
  -e      : provide the full executable path (normally in quotes), reworked and specifies the command to run the game
  -m      : max auto saves (default 50). Use 0 for unlimited
  -d      : debug output
  -t      : trace output

Examples:
    python dbh-savestate-manager.py

Output:

    q) quit the script
    d) open autosave folder in dolphin
    0) Continue from most recent
    1) Chapter1 Big Choice
    2) $2021-06-16 22:45:39
    3) $2021-06-16 22:22:23
    4) $2021-06-16 22:17:49
    5) $2021-06-16 22:12:43
    6) $2021-06-16 22:07:36
    7) $2021-06-16 22:02:30
    8) $2021-06-16 21:57:23
    9) Next
    
    Select Option:

Details:

  Script kicks off the game and begins a timer. Every 5 minutes it makes a
  snapshot of the games save directory and places it in the session
  autosave folder using a timestamp as the name.

  When you start the game with this program as the bootstrap, you will be
  presented with a list of timestamps. If you select one that is not
  the latest, you will be prompted if you want to rename the session
  before loading it. Then script kicks off the game.

  For space reasons, this script maintains at most 50 saves. It then ages
  off older saves. It does not age off any folders that you rename.

Contacts:
    This script was based on Session AutoSave Manager and ported to Linux by Andrew Shark.
    For bug reports or feature requests visit repository: https://gitlab.com/AndrewShark/dbh-savestate-manager
    ''')

    sys.exit(1)

import sched
import time
import sys
import os
import math
# import string
# import struct
import shutil
import subprocess
from pathlib import Path

##############################################################################
# GLOBAL VARIABLES
##############################################################################
gameExe       = ''
threadManager = None
gameSaveDir   = ''
autoSaveDir   = ''
nextSaveTime  = 0
saveFrequency = 300
stillRunning  = True
maxSaves      = 50
sessionName   = 'default'
    
debug   = False
trace   = False

##############################################################################
# GLOBAL SUPPORT METHODS
##############################################################################
    
def startgame():
    global gameExe
    global debug
    
    try:
        if debug:
            print("  [DEBUG] Starting [%s] at [%s]" % (gameExe, human_time(math.floor(time.time()))))
        print('[INFO] Starting Game')
        startgame_cmd=gameExe.split()
        completed = subprocess.run(startgame_cmd)

        print("Waiting several seconds, prepare to launch window should appear")
        time.sleep(10)

        # Note: There are two scenarios when launching game: short and long. The prepare to launch window (a small grey steam window) will always appear. The short scenario is when it disappears quickly.
        # A long scenario is when it starts Vulkan shader compilation process in that window.
        # I still did not understand what triggers the vulkan shader compilation. I guess that it may be some time length after previous launch.
        # One thing to try to trigger long scenario is quit steam application, then start it again. However, sometimes the game still launches by quick scenario even after relaunching steam.

        found_window = find_prepare_to_launch_window()
        while found_window:
            # Wait until it disappears. Otherwise, script thinks that game is ended before it even started.
            print("Waiting until prepare to launch window disappears")
            time.sleep(10)
            found_window = find_prepare_to_launch_window()
        print("Prepare to launch window does not exist, proceeding")
        #  Because game window actually appears only after several seconds, and proceeding without waiting causes script to think the game has ended
        print("Waiting several seconds after prepare to launch window disappeared and before the actual game window will appear")
        time.sleep(30)

    except:
        print('[ERROR] Unable to execute [%s]' % (gameExe))
        if debug:
            print(sys.exc_info()[0])

def find_prepare_to_launch_window():
    windows_list = Window.list()
    windows_steam = [ window for window in windows_list if window.wm_class == "Steam.Steam" ]
    # window_dbh_prepare = [window for window in windows_list if window.wm_name == "Detroit: Become Human — Steam" ]  # not working for some reason, see comment below.

    # For some reason, in python script I cannot get the window with wm_name == "Detroit: Become Human — Steam".
    # Maybe because it is some kde magic tricks that show names of windows for window specific rules (this is where I copied that string from)?
    # Or maybe wmctrl gets it incorrectly from the system (maybe specifically in kde?)? Need to figure that out.
    # Currently, what I see in windows with wm.class "Steam.Steam" that that prepare window (of course after waiting when it gets such header) in its wm_name is N/A.

    # I think it could be rather fragile (but at least it works for me now), but I will detect this window by its height and width.
    window_dbh_prepare = [window for window in windows_steam if window.h == 240 and window.w == 384 ]

    if not window_dbh_prepare:
        return False
    else:
        return True

def human_time(val):
    # Get int value, convert it to formatted string
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(val))

def monitor():
    global nextSaveTime
    global saveFrequency
    global threadManager
    global gameSaveDir
    global autoSaveDir
    global maxSaves
    global debug
    global trace
    
    gameRunning = isRunning('DetroitBecomeHuman.exe')
    now = math.floor(time.time())
    
    # Is it time to autosave?
    if now > nextSaveTime or not gameRunning:
        nextSaveTime = math.floor(time.time()) + saveFrequency
        if trace:
            print("    [TRACE] Autosaving at [%s] nextSaveTime [%s]" % (human_time(now), human_time(nextSaveTime)))
        if  len(listdirbydate(gameSaveDir)) > 0:
            destFile = time.strftime('$%Y-%m-%d %H:%M:%S')
            destPath = os.path.join(autoSaveDir,destFile)
            if not os.path.isdir(destPath):
                try:
                    shutil.copytree(gameSaveDir, destPath)
                    print('[INFO] Saved Game : %s' % (destFile))
                except:
                    print('[ERROR] unable to copy [%s] to [%s]' % (gameSaveDir, destPath))
                    print(sys.exc_info()[0])        
                cleanup()
            elif debug:
                print('  [DEBUG] path already exists [%s]' % (destPath))        
        elif debug:
            print('  [DEBUG] No save yet exists [%s]' % (gameSaveDir))
            
    if gameRunning:
        if trace:
            print('    [TRACE] Game is running [%s] nextSaveTime [%s]' % (human_time(now), human_time(nextSaveTime)))
        threadManager.enter(8, 1, monitor)
    else:
        if trace:
            print("    [TRACE] Game has ended [%s]" % (human_time(now)))
        else:
            print("[INFO] Game is Not Running. Stopping Auto Save Monitor")
        pauseAndExit()

def selectOne(optList, prompt, offset=0):
    print("q) quit the script")
    print("d) open autosave folder in dolphin")
    if 0 == offset:
        print('0) Continue from most recent')
    else:
        print('0) Prev')

    maxOffset = offset+8
    if maxOffset > len(optList):
        maxOffset = len(optList)
    lastOpt = (maxOffset - offset + 1)        
    for i in range(0,lastOpt-1):
        print('%d) %s' % ((i + 1), optList[offset + i]))
        
    if 9 == lastOpt:
        print('%d) Next' % (lastOpt))
        
    r = getInput(prompt, '')

    if r in ['q']:
        return -2
    if r in ['d']:
        return -3
    if r in ['0','1','2','3','4','5','6','7','8','9']:
        r = int(r)
        if trace:
            print('    [TRACE] Selected [%d]' % (r))
        if r > lastOpt:
            return selectOne(optList, prompt, offset)        
        if r == lastOpt:
            if len(optList) > maxOffset:
                return selectOne(optList, prompt, maxOffset)
            return selectOne(optList, prompt, offset)
        elif trace:
            print('    [TRACE] Not Last [%d]' % (r))
                
        if 0 == r:
           if 0 == offset:
               return -1
           return selectOne(optList, prompt, offset - 8)
        return (offset + (r - 1))  
    return -1

def getInput(msg, default):
    """ get input in manner that is compatible with Python 2/3 and when script is converted to exe """
    global trace
    
    sys.stdout.write(msg)
    sys.stdout.flush()
    
    response = '#\t'
    try:
        # This works regardless of python version when script is converted to exectuable, but only 
        # works for Python 2.x when ran as a script.
        response = raw_input()
    except:
        pass
    if '#\t' == response:
        try:
            # This ONLY works for Python 3.x when ran from script (If script converted to exe, it fails).
            response = input()
        except:
            pass
    if '#\t' == response:
        return default
    return response

def isRunning(procName):
    global debug

    for p in psutil.process_iter(["cmdline"]):
        if p.info['cmdline'] and procName in p.info['cmdline'][0]:
            return True
    return False


def cleanup():
    global autoSaveDir
    global maxSaves
    global debug
    global trace
    
    # Sanity
    if maxSaves < 1:
        if trace:
            print('    [TRACE] maxSaves is [%d]. Skipping Cleanup' % (maxSaves))
        return    
    if 0 == len(autoSaveDir):
        if trace:
            print('    [TRACE] autoSaveDir not initialized. Skipping Cleanup')
        return
    if autoSaveDir.find('Quantic Dream') < 0:
        if trace:
            print('    [TRACE] autoSaveDir [%s] does not contain [Quantic Dream]. Bailing...')
        return
    
    # Ignore custom named saves. They do not count when enforcing max saves...
    autoSaveGames = [f for f in listdirbydate(autoSaveDir) if f.startswith('$')]
    currentSaves = len(autoSaveGames)
    if currentSaves > maxSaves:
        if trace:
            print('    [TRACE] Current Saves [%d] > maxSaves [%s]' % (currentSaves, maxSaves))
    
        # Remove an extra 10% so we aren't deleting every time we run. 
        numToRemove = (currentSaves - maxSaves) + (maxSaves // 10)
        
        # By default, they should be ordered oldest (first) to newest (last)
        # It is just a matter of popping numToRemove off and deleting the 
        # directories.        
        for i in range(0,numToRemove):
            oldSavePath = os.path.join(autoSaveDir,autoSaveGames[i])
            if trace:
                print('    [TRACE] Removing [%s]' % (oldSavePath))
            try:
                shutil.rmtree(oldSavePath)
            except:
                if trace:
                    print('    [TRACE] Exception Removing [%s]' % (oldSavePath))
                    print(sys.exc_info()[0])
            
            if trace:
                if not os.path.isdir(oldSavePath):      
                    print('    [TRACE] [%s] Removed' % (oldSavePath))
                else:
                    print('    [TRACE] Failed to Delete [%s]' % (oldSavePath))


    elif trace:
        print('    [TRACE] Current Saves [%d] =< maxSaves [%s]' % (currentSaves, maxSaves))    

def loadGame(saveName, srcPath, silent=True):
    global gameSaveDir
    global trace
    
    if os.path.isdir(srcPath):
        # Sanity Check:
        if len(gameSaveDir) > 0 and gameSaveDir.find('Detroit Become Human') > 0:
            try:
                if not silent:
                    print('[INFO] Loading Save : %s' % (saveName))
                if trace:
                    print('    [TRACE] Removing [%s]' % (gameSaveDir))
                shutil.rmtree(gameSaveDir)
                if not os.path.isdir(gameSaveDir):
                    if trace:
                        print('    [TRACE] Save Game Successfully Removed')
                        print('    [TRACE] Copying [%s] to [%s]' % (srcPath, gameSaveDir))
                    shutil.copytree(srcPath, gameSaveDir)
                    return True
                else:
                    print('[ERROR] Unable to Delete [%s]' % (gameSaveDir))                        
            except:
                print('[ERROR] unable to copy [%s] to [%s]' % (srcPath, gameSaveDir))
                print(sys.exc_info()[0])
    return False
    
def getOldestFileTimeStamp(file_abs):
    if os.path.getctime(file_abs) < os.path.getmtime(file_abs):
        return os.path.getctime(file_abs)
    return  os.path.getmtime(file_abs)

def containsAny(p_str,p_set):
    return 1 in [c in p_str for c in p_set]
    
def listdirbydate(dir):
    if not os.path.isdir(dir):
        return []
    return [f.parts[-1] for f in sorted(Path(dir).iterdir(), key=os.path.getctime)]        

def pauseAndExit():
    try:
        print('')
        input("Press any key to continue...")
    except:
        pass
    sys.exit(1)
    
NOTALLOWED = set([':','*','?','"','<','>','|','\\','/'])

##############################################################################
# MAIN
##############################################################################

def run(args):
    global gameExe
    global threadManager
    global gameSaveDir
    global autoSaveDir
    global nextSaveTime
    global saveFrequency
    global maxSaves
    global debug
    global trace
    global sessionName
        
    if '-h' in args:
        overview()

    if '-t' in args:
        trace = True
        debug = True
    elif  '-d' in args:
        debug = True


    if '-s' in args:
        try:
            sName   = args[args.index('-s')+1]
            if containsAny(sName, NOTALLOWED):
                print('[ERROR] Session Name [%s] Invalid. The following characters are not allowed [%s]' % (','.join(NOTALLOWED)))
            elif sName.find('..') > -1:
                print('[ERROR] Session Name [%s] Invalid. The sequence [..] is not allowed')
            else:
                sessionName = sName                        
        except:
            if debug:
                print('[DEBUG] Error Parsing sessionName parameter')    
    
    print('==============================================================================')
    print('                 Detroit: Become Human savestate manager                      ')
    print('                         Session name: [%s]' % (sessionName))
    print('==============================================================================')
    
    if trace:
        print("[INFO] Trace Output Enabled")
    elif debug:
        print("[INFO] Debug Output Enabled")
    
    
    if '-t' in args:
        print("[INFO] Trace Output Enabled")
        trace = True
        debug = True
    elif  '-d' in args:
        print("[INFO] Debug Output Enabled")
        debug = True

    saveMinutes = saveFrequency // 60
    if '-f' in args:
        try:
            saveMinutes   = int(args[args.index('-f')+1])
            saveFrequency = saveMinutes * 60
        except:
            if debug:
                print('Error Parsing frequency parameter')
            pass
            
    print('[INFO] Auto Save every : [%d] minute(s)' % (saveMinutes))
    if '-m' in args:
        try:
            maxSaves       = int(args[args.index('-m')+1])
        except:
            if debug:
                print('Error Parsing max saves parameter')
            pass
    if maxSaves < 1:
        print('[INFO] Maximum Saves   : [unlimited]')
    else:
        print('[INFO] Maximum Saves   : [%d]' % (maxSaves))

    # Confirm userprofile is known (if not, we can't autosave anyway)
    userprofiledir =  "/home/" + os.getenv('USER') + "/.local/share/Steam/steamapps/compatdata/1222140/pfx/drive_c/users/steamuser/"
    if not userprofiledir:
        print("[INFO] Required Environment variable USER not set.")
        pauseAndExit()
    # Sanity
    if userprofiledir.find('..') > -1:
        print("[ERROR] User Profile contains unsafe char sequence [..] Bailing...")
        pauseAndExit()

    # Confirm Game Save Dir:
    gameSaveDir = os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream','Detroit Become Human')
    if not os.path.isdir(gameSaveDir):
        if debug:
            print('  [DEBUG] Game does not appear to have any save games')
        try:
            if not os.path.isdir(os.path.join(userprofiledir, 'Saved Games')):
                os.mkdir(os.path.join(userprofiledir, 'Saved Games'))
            if not os.path.isdir(os.path.join(userprofiledir, 'Saved Games')):
                print('[INFO] Unable to create Save Game directory [%s]. Run game manually and try again' % (os.path.join(userprofiledir, 'Saved Games')))        
                pauseAndExit()
            if not os.path.isdir(os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream')):
                os.mkdir(os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream'))
            if not os.path.isdir(os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream')):
                print('[INFO] Unable to create Save Game directory [%s]. Run game manually and try again' % (os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream')))
                pauseAndExit()    
            if not os.path.isdir(gameSaveDir):
                os.mkdir(gameSaveDir)
        except:
            if debug:
                print('  [DEBUG] Exception thrown creating save directory')
            pass
                        
        if not os.path.isdir(gameSaveDir):
            print('[INFO] Unable to create Save Game directory [%s]. Run game manually and try again' % (gameSaveDir))
            pauseAndExit()

    # Confirm autosaves parent directory:
    autoSaveParentDir = os.path.join(userprofiledir, 'Saved Games', 'Quantic Dream', 'autosaves')
    if not os.path.isdir(autoSaveParentDir):
        print('[INFO] Creating autosaves dir [%s]' % (autoSaveParentDir))
        try:
            os.mkdir(autoSaveParentDir)
            if not os.path.isdir(autoSaveParentDir):
                print('[INFO] Unable to create autosaves directory [%s]' % (autoSaveParentDir))
                pauseAndExit()
        except:
            if debug:
                print('  [DEBUG] Exception thrown creating autosaves directory')
                print(sys.exc_info()[0])

    sessionList = listdirbydate(autoSaveParentDir)
    if trace:
        print('    [TRACE] Number of Sessions [%d]' % (len(sessionList)))
        for s in sessionList:
            print('    [TRACE] session [%s]' % (s))


    # Confirm autosave directory:
    autoSaveDir = os.path.join(autoSaveParentDir, sessionName)    
    if not os.path.isdir(autoSaveDir):
        print('[INFO] Creating autosave dir for session [%s]' % (sessionName))
        try:
            os.mkdir(autoSaveDir)
        except:
            if debug:
                print('  [DEBUG] Exception thrown creating session directory')
                print(sys.exc_info()[0])
            pass            
        if not os.path.isdir(autoSaveDir):
            print('[INFO] Unable to create session dir [%s] Check permissions and try again' % (autoSaveDir))
            pauseAndExit()            
    
    if isRunning('DetroitBecomeHuman.exe'):
        print('[INFO] Game is Running. Starting AutoSave Monitor')
        nextSaveTime = math.floor(time.time()) + saveFrequency
        threadManager = sched.scheduler(time.time, time.sleep)
        threadManager.enter(8, 1, monitor)
        threadManager.run()
        return

    if not isRunning("steam"):
        # This is needed because otherwise, when steam launched by just invoking game starting command, the script waits until you quit steam.
        print("Steam is not launched. Launching steam and waiting several seconds")
        subprocess.Popen(['steam'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, start_new_session=True)
        time.sleep(20)

    gameExe = "steam steam://rungameid/1222140"
        
    if debug:
        print("  [DEBUG] Using executable [%s]" % (gameExe))      
        
    # Get list of saved games
    savegames = []
    for subdir in listdirbydate(autoSaveDir):
        if subdir.startswith('.'):
            continue
        subdirFullPath = os.path.join(autoSaveDir,subdir)
        if os.path.isdir(subdirFullPath):
            savegames.append(subdir)
            
    loadedSave = False
    if 0 != len(savegames):
        print('[INFO] Current Saves   : [%d]' % (len(savegames)))
        savegames.reverse()
        # Prompt user for restore point
        print('')
        action = selectOne(savegames, '\nSelect Option (just hit enter to use current save): ')
        if action > -1:
            saveName = savegames[action]
            if trace:
                print('    [TRACE] Selected [%s]' % (saveName))
            srcPath = os.path.join(autoSaveDir,saveName)
            if saveName.startswith('$'):
                chose = getInput('Do you wish to rename [%s] (y/N): ' % (saveName), 'n')
                if 'y' == chose:
                    newName = getInput('Enter New Name: ', saveName)
                    if newName == saveName:                 
                        print('Rename aborted')
                    elif containsAny(newName, NOTALLOWED):
                        print('Rename aborted. The following characters are not allowed [%s]' % (','.join(NOTALLOWED)))
                    elif newName.find('..') > -1:
                        print('Rename aborted. The sequence [..] is not allowed')
                    else:
                        trgPath = os.path.join(autoSaveDir,newName)
                        if not os.path.isdir(trgPath):
                            try:
                                shutil.move(srcPath, trgPath)
                            except:
                                if trace:
                                    print('    [TRACE] Exception renaming directory')
                                    print(sys.exc_info()[0])
                            if os.path.isdir(trgPath):
                                print('Save [%s] renamed to [%s]' % (saveName,newName))
                                srcPath  = trgPath
                                saveName = newName                               
                        else:
                            print('Rename aborted. Path [%s] already exists' % (trgPath))
            loadedSave = loadGame(saveName, srcPath, False)

        else:
            if action == -2:
                print('Quitting the script')
                pauseAndExit()
            if action == -3:
                print("Opening autosave folder in dolphin")
                dolphin_url = userprofiledir + "Saved Games/Quantic Dream/autosaves/"
                subprocess.run(["dolphin", dolphin_url])
                pauseAndExit()
            elif trace:
                print('    [TRACE] Selected Nothing/Skip')
            
    elif sessionName != 'default' and len(sessionList) > 1:
    
        # Special Case: They specified a custom session, a default already 
        # exists and this is our first run. Clear the current Save Game
        # The sessionList length check is important, otherwise users could
        # accidently blow away their 1 save game when first running this.
        
        if len(gameSaveDir) > 0 and gameSaveDir.find('Detroit Become Human') > 0:
            try:
                if trace:
                    print('    [TRACE] Removing [%s]' % (gameSaveDir))
                shutil.rmtree(gameSaveDir)
                if not os.path.isdir(gameSaveDir):
                    if trace:
                        print('    [TRACE] Save Game Successfully Removed')
                    os.mkdir(gameSaveDir)
            except:
                print('[ERROR] unable to clear Game Save Cache for new session')
                print(sys.exc_info()[0])
    
        
    if len(sessionList) > 1 and not loadedSave:
    
        print('[INFO] Restoring Last Known Save for Session [%s]' % (sessionName))
        
        # As rule, if there is only 1 session, no need to do anything during 
        # default start up. But if there are 2 or more sessions, we don't know if they 
        # were using the other session before. So we have to restore the last 
        # known save state in that case:
    
        autoSaveGames = [f for f in listdirbydate(autoSaveDir) if f.startswith('$')]
        if len(autoSaveGames) > 0:
            saveName = autoSaveGames[-1]
            if trace:
                print('    [TRACE] Restoring Last Session Save [%s]' % (saveName))
            srcPath = os.path.join(autoSaveDir,saveName)
            if not loadGame(saveName, srcPath):
                print('[ERROR] Unable to restore last save game [%s]' % (saveName))    
                
    # Start game/monitor:
    print('[INFO] Starting AutoSave Monitor')
    nextSaveTime = math.floor(time.time()) + saveFrequency
    threadManager = sched.scheduler(time.time, time.sleep)
    threadManager.enter(1, 1, startgame)        
    threadManager.enter(45, 1, monitor)    
    threadManager.run()
                
##############################################################################
# Command Line
##############################################################################    
run(sys.argv[1:])
